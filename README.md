# Adonis: Request Life Cycle
An Adonis application that mocks the CRUD operations

# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

### Endpoints
To test the endpoints on PostMan, go [here](https://www.getpostman.com/collections/b3a267b80bc3b15cd9af)

###### GET: [Fetch all interns: http://127.0.0.1:3333/interns](http://127.0.0.1:3333/interns)

###### GET: [Fetch single intern: http://127.0.0.1:3333/interns/{id:int}](http://127.0.0.1:3333/interns/{id:int})

###### POST: [Create new user: http://127.0.0.1:3333/register](http://127.0.0.1:3333/register)

###### DELETE: [Delete single user: http://127.0.0.1:3333/interns/{id:int}](http://127.0.0.1:3333/interns/{id:int})

###### UPDATE [Update user: http://127.0.0.1:3333/interns/{id:int}](http://127.0.0.1:3333/interns/{id:int})
