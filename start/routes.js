'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')

//Register intern
Route.post('/register', 'InternController.register')

//Fetches all interns
Route.get('/interns', 'InternController.showall')

//Fetches a single intern
Route.get('/interns/:id', 'InternController.showone')


//Updates intern details
Route.put('/interns/:id', 'InternController.update')

// Deletes a specific intern
Route.delete('/interns/:id', 'InternController.destroy')


