'use strict'


//Interns list
var interns = [
{id: 1, name: 'Dorcas Cherono', email: 'dorcas@sendyit.com', stacks: ['Vue JS', 'Adonis']},
{id: 2, name: 'Gill Erick', email: 'erick@sendyit.com', stacks: ['Flutter', 'Quarkus']},
{id: 3, name: 'Maxwell Kiprop', email: 'maxwell@sendyit.com', stacks: ['Vue JS', 'Golang']},
{id: 4, name: 'Stacey Chebet', email: 'schebet@sendyit.com', stacks: ['Flutter', 'Adonis']}
]

const {validate} = use('Validator')

class InternController{
  //Returns existing interns
  async showall({response}){
      response.header('Content-Type' ,'application.json')
      return response.json({
        interns
      })
    }

  //Display intern of ID: id
  async showone({response, params: {id}}){
    var intern = interns.find(intern => intern.id == id);
    if (intern == null){
      return response.status(404).json({error: "User Not Found"})

    }
    else {
      // response.header('Content-Type', 'application.json')
      return response.json({
        intern
      })
    }
  }

  //Creates new intern and returns interns array
  async register ({request, session, response}) {
    const rules = {
      email: 'required|email',
      name: 'required|string',
      stacks: 'required|string'
    }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {session.withErrors(validation.messages())

      return response.status(403).json({Error: "Wrong Input Format"})
    }
    let userRequest = request.only(['name', 'email', 'stacks'])

    const newIntern = {
      id: userRequest.id = interns.length + 1,
      email: userRequest.email,
      name: userRequest.name,
      stacks: userRequest.stacks
    }

    interns.push(newIntern)
    response.header('Content-Type', 'application.json')
    return response.json({message: "New User Created",data: interns})
  }

  //Deletes a specific intern & returns new interns array
  async destroy({response, params: {id}}){
    var intern = interns.find(intern => intern.id == id);
    if (!intern){
      return response.status(404).json({Error:"User Does Not Exist"})
    }

    var i = interns.indexOf(intern)
    interns.splice(i, 1)
    response.header('Content-Type', 'application.json')
    return response.json({
      message: `UserID ${id} Deleted`,
      data: interns})
  }

  //Updates a specific intern's details
  async update({request, session, response, params: {id}}){
    const rules = {
      email: 'email',
      name: 'string',
      stacks: 'string'
    }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {session.withErrors(validation.messages())

      return response.status(403).json({Error: "Wrong Input Format"})
    }
    var intern = interns.find(intern => intern.id == id);
    let userRequest = request.only(['name', 'email', 'stacks'])

    //Modify optional fields
    if (userRequest.email != null){
      intern.email = userRequest.email
    }else if (userRequest.name != null){
      intern.name = userRequest.name
    }else if(userRequest.stacks != null){
      intern.stacks = userRequest.stacks
    }
    else if (userRequest.id != null){
      return response.status(403).send({message: 'UserID cannot be updated'})
    }
    response.header('Content-Type', 'application.json')
    return response.json({
      message: `User successfully updated`,
      data: intern})
  }
}

module.exports = InternController
